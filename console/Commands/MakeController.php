<?php namespace Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MakeController extends Command
{
    protected function configure()
    {
        $this
            ->setName('make:controller')
            ->setDescription('Haplo will create a simple controller class for you')
            ->addArgument(
                'ControllerName',
                InputArgument::REQUIRED,
                'Enter name for new controller:'
            )
            ->addOption(
                'clear',
                null,
                InputOption::VALUE_NONE,
                'If set, the task create an empty controller'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ControllerName = $input->getArgument('ControllerName');

        $phpFile =
            <<<EOF
<?php namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;


/**
 * Class $ControllerName
 * This class is used for
 * ex:
 */
class $ControllerName extends Controller {

    public function index()
    {
        // TODO: Write index method
    }

    public function store()
    {
        // TODO: Write index method
    }

    public function update()
    {
        // TODO: Write index method
    }

    public function remove()
    {
        // TODO: Write index method
    }

    public function other(Request \$req, Response \$res, \$args)
    {
        // TODO: Write other method
    }

}
EOF;

        if ($input->getOption('clear')) {
        }

        $fh = fopen(__DIR__.'/../../app/Controllers/'.$ControllerName.'.php', 'x') or die("Can't create file");
        fwrite($fh, $phpFile);
        fclose($fh);
        $output->writeln($ControllerName.'.php Was created successfully!');
    }
}