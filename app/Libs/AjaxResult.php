<?php namespace App\Libs;
/**
 * Created by PhpStorm.
 * User: demiand
 * Date: 1/18/2016
 * Time: 7:18 AM
 */
class AjaxResult {

    private $errors = array();
    private $result = array();

    public function __construct($result=array(), $errors=array()){
        $this->result = $result;
        $this->errors = $errors;
    }

    public function getErrors(){
        return $this->errors;
    }

    public function hasError(){
        return count($this->errors) > 0;
    }

    public function getResult(){
        return $this->result;
    }

    public function setResult($result){
        $this->result = $result;
        return $this;
    }

    public function addError($error, $log = false){
        if ($log) {
            \Log::ERROR($error);
        }
        $this->errors[] = $error;
        return $this;
    }

    public function addErrorAssoc($key, $value){
        $this->errors[$key] = $value;
        return $this;
    }

    public function clearError(){
        $this->errors = array();
        return $this;
    }

    public function clearResult(){
        $this->result = array();
        return $this;
    }

    public static function create($result=array(), $errors=array()){
        return new AjaxResult($result, $errors);
    }

    public function toJSON(){
        $out = array();
        if($this->hasError()){
            $out['err'] = 1;
            $out['errors'] = $this->getErrors();
        } else{
            $out['err'] = 0;
        }
        $out['result'] = $this->result;
        return json_encode($out);
    }

    public function __toString(){
        return $this->toJSON();
    }

}