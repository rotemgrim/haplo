<?php


class Log
{
    static $instance;

    public static function init(\Monolog\Logger $logger){
        if(self::$instance === null)
            self::$instance = $logger;
    }

    private static function addRecord($addType, $text, array $data){
        self::$instance->$addType($text, $data);
    }

    public static function __callStatic($addType, $args){
        self::addRecord($addType, $args[0], $args[1]);
    }

    public static function DEBUG($text, array $data=[]){self::addRecord(__FUNCTION__, $text, $data);}
    public static function INFO($text, array $data=[]){self::addRecord(__FUNCTION__, $text, $data);}
    public static function NOTICE($text, array $data=[]){self::addRecord(__FUNCTION__, $text, $data);}
    public static function WARNING($text, array $data=[]){self::addRecord(__FUNCTION__, $text, $data);}
    public static function ERROR($text, array $data=[]){self::addRecord(__FUNCTION__, $text, $data);}
    public static function CRITICAL($text, array $data=[]){self::addRecord(__FUNCTION__, $text, $data);}
    public static function ALERT($text, array $data=[]){self::addRecord(__FUNCTION__, $text, $data);}
    public static function EMERGENCY($text, array $data=[]){self::addRecord(__FUNCTION__, $text, $data);}
}