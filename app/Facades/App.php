<?php


class App
{
    /**
     * @property-read array settings
     * @property-read \Slim\Interfaces\Http\EnvironmentInterface environment
     * @property-read \Psr\Http\Message\ServerRequestInterface request
     * @property-read \Psr\Http\Message\ResponseInterface response
     * @property-read \Slim\Interfaces\RouterInterface router
     */
    private static $instance;
    private static $request;
    private static $response;
    private static $arguments;
    public  static $settings;

    public static function preInit(Slim\Container &$app){
        if(self::$instance === null){
            self::$instance = &$app;
            self::$settings = self::$instance->settings;
            self::$request  = self::$instance->request;
            self::$response = self::$instance->response;
        }
    }

    public static function init( \Slim\Http\Request &$req, \Slim\Http\Response &$res){
        self::$request = $req;
        self::$response = $res;
    }

    public static function request(){
        $tmp =  \Slim\Http\Request::createFromEnvironment(self::$instance->environment);
        $tmp = self::$request;
        return $tmp;
    }

    public static function response(){
        $tmp = new \Slim\Http\Response();
        $tmp = self::$response;
        return $tmp;
    }

    public static function arguments(){
        return (array) self::$request->getAttributes();
    }

    public static function router(){
        $tmp = new \Slim\Router();
        $tmp = self::$instance->router;
        return $tmp;
    }
    public static function environment(){
        $tmp = new \Slim\Http\Environment();
        $tmp = self::$instance->environment;
        return $tmp;
    }
    public static function hash(){
        $tmp = new \Hashids\Hashids();
        $tmp = self::$instance->hash;
        return $tmp;
    }

    public static function hash_it($var){
        return md5(md5($var) . self::$instance->get('settings')['hash']['salt']);
    }

    public static function getIp(){
        return self::$request->getAttribute('ip_address');
    }

    private static function mediator($funcName, $args){
        return call_user_func_array([self::$instance,$funcName], $args);
    }

    public static function __callStatic($funcName, $args){
        return self::mediator($funcName, $args);
    }


}