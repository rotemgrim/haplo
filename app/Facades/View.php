<?php


class View
{
    private static $instance;
    private  $inst;

    public function __construct(Philo\Blade\Blade $view){
        $this->inst = $view;
    }
    public static function init(Philo\Blade\Blade $view){
        if(self::$instance === null)
            self::$instance = $view;
    }

    private static function mediator($funcName, $args){
        return call_user_func_array([self::$instance->view(),$funcName], $args);
    }

    public static function __callStatic($funcName, $args){
        return self::mediator($funcName, $args);
    }

    public function __invoke($a){
        $args = func_get_args();
        return call_user_func_array([$this->inst->view(),'make'], $args);
        //return self::mediator(__FUNCTION__, $args);
    }
    public static function make(){
        $args = func_get_args();
        return self::mediator(__FUNCTION__, $args);
    }

}