<?php


class DB
{
    private static $connection;

    public static function init(\Doctrine\DBAL\Connection $conn){
        if(self::$connection === null)
            self::$connection = $conn;
    }

    private static function mediator($funcName, $args){
        return call_user_func_array([self::$connection,$funcName], $args);
    }

    public static function __callStatic($funcName, $args){
        return self::mediator($funcName, $args);
    }

    public static function query(){
        $args = func_get_args();
        if(func_num_args() > 1)
            return call_user_func_array([self::$connection, 'executeQuery'], $args);
        return self::mediator(__FUNCTION__, $args);
    }

    // Functions from interface
    public static function prepare($prepareString){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function quote($input, $type=\PDO::PARAM_STR){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function exec($statement){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function lastInsertId($seqName = null){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function beginTransaction(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function commit(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function rollBack(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function errorCode(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function errorInfo(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}

    // Functions from connection
    public static function getParams(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getDatabase(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getHost(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getPort(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getUsername(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getPassword(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getDriver(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getConfiguration(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getEventManager(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getDatabasePlatform(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getExpressionBuilder(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function connect(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function isAutoCommit(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function setAutoCommit($autoCommit){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function setFetchMode($fetchMode){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function fetchAssoc($statement, array $params = array(), array $types = array()){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function fetchArray($statement, array $params = array(), array $types = array()){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function fetchColumn($statement, array $params = array(), $column = 0, array $types = array()){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function isConnected(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function isTransactionActive(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function delete($tableExpression, array $identifier, array $types = array()){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function close(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function setTransactionIsolation($level){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getTransactionIsolation(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function update($tableExpression, array $data, array $identifier, array $types = array()){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function insert($tableExpression, array $data, array $types = array()){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function quoteIdentifier($str){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function fetchAll($sql, array $params = array(), $types = array()){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function executeQuery($query, array $params = array(), $types = array(), QueryCacheProfile $qcp = null){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function executeCacheQuery($query, $params, $types, QueryCacheProfile $qcp){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function project($query, array $params, Closure $function){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function executeUpdate($query, array $params = array(), array $types = array()){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getTransactionNestingLevel(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function transactional(Closure $func){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function setNestTransactionsWithSavepoints($nestTransactionsWithSavepoints){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getNestTransactionsWithSavepoints(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function createSavepoint($savepoint){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function releaseSavepoint($savepoint){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function rollbackSavepoint($savepoint){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getWrappedConnection(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function getSchemaManager(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function setRollbackOnly(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function isRollbackOnly(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function convertToDatabaseValue($value, $type){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function convertToPHPValue($value, $type){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function resolveParams(array $params, array $types){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function createQueryBuilder(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}
    public static function ping(){$args = func_get_args();return self::mediator(__FUNCTION__, $args);}

}