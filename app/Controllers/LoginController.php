<?php namespace App\Controllers;

use App\Libs\AjaxResult;
use App\Models\DataMappers\User;
use duncan3dc\Sessions\Session;
use Slim\Http\Request;
use Slim\Http\Response;


/**
 * Class LoginController
 * This class is used for login into admin panel
 *
 */
class LoginController extends Controller {

    public function index()
    {
        if(User::isLogged())
            return \App::response()->withRedirect((string) \App::router()->pathFor('dashboard'), 302);
        return \App::response()->write(\View::make('pages.login'));
    }

    public function store()
    {
        // Set up empty ajax result
        $ajax = new AjaxResult();

        // check if user credentials are valid
        $user = new User();
        if(!$user->validate(\App::request())){

            $ajax->addError('User was not found');

        } else {
            // Set up session for backend
            $backend = Session::createNamespace('backend');
            $backend->clear();

            // Save user obj into session
            $backend->set('user', $user);

            // Return Dashboard url for js redirect in login.blade
            $ajax->setResult(['destination' => \App::router()->pathFor('dashboard')]);
        }
        return $ajax->__toString();
    }

}