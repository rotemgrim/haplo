<?php namespace App\Controllers;

use App\Models\DataMappers\User;
use duncan3dc\Sessions\Session;
use Slim\Http\Request;
use Slim\Http\Response;


/**
 * Class LogoutController
 * This class is used for logginout the user
 * ex: http://localhost/logout
 */
class LogoutController extends Controller {

    public function index()
    {
        User::logout();
        echo 'User logged out.';
    }

}