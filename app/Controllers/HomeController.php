<?php namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;


/**
 * Class HomeController
 * This class is used for
 * ex:
 */
class HomeController extends Controller {

    public function index()
    {
        //\Log::ALERT('test');
        //return new Response(200,null,\View::make('pages.homepage', ['name'=>'rotem']));
        return \View::make('pages.homepage', ['name'=>'rotem'])->__toString();

    }

    public function store()
    {
        // TODO: Write index method
    }

    public function update()
    {
        // TODO: Write index method
    }

    public function remove()
    {
        // TODO: Write index method
    }

    public function other(Request $req, Response $res, $args)
    {
        // TODO: Write other method
    }

}