<?php namespace App\Controllers;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class Controller {

    protected $app;
    protected static $slim;

    public function __construct(Container $app)
    {
        $this->app = $app;
        self::$slim=$app;
    }

    public function __invoke(Request $req, Response $res, $args){
        \App::init($req, $res);
        switch($req->getMethod()){
            case "GET":     return $this->index(); break;
            case "POST":    return $this->store(); break;
            case "PUT":     return $this->update(); break;
            case "DELETE":  return $this->remove(); break;
        }
    }

    private function methodNotFoundMsg($method){
        \Log::ALERT($method . ' not found in '. get_class($this));
        return \App::response()->withStatus(500);
    }

    protected function index() {return $this->methodNotFoundMsg(__FUNCTION__);}
    protected function store() {return $this->methodNotFoundMsg(__FUNCTION__);}
    protected function update(){return $this->methodNotFoundMsg(__FUNCTION__);}
    protected function remove(){return $this->methodNotFoundMsg(__FUNCTION__);}

    public function __toString(){
        return get_class($this);
    }
}