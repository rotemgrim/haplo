<?php


$app->get('/', 'App\Controllers\HomeController')->setName('homepage');

$app->get('/link/{slug}', 'App\Controllers\LinkController')->setName('link');
$app->get('/click/{clickHash}', 'App\Controllers\ClickController')->setName('link');
$app->get('/logout', 'App\Controllers\LogoutController')->setName('logout');

// FOR ADMIN PANEL:
$app->map(['GET', 'POST'], '/login', 'App\Controllers\LoginController')->setName('login');

$app->group('/admin', function() use ($app){

    $app->get('/dashboard', 'App\Controllers\DashboardController')->setName('dashboard');
    $app->get('/links', 'App\Controllers\LinksController')->setName('links');
    $app->get('/edit/link/{id}', 'App\Controllers\LinksController:edit');

})->add($adminAuthMiddleware);

// THE REAL MAGIC:
$app->map(['GET','POST'], '/pixel-lead', 'App\Controllers\PixelController:lead')->setName('pixel-lead');
$app->map(['GET','POST'], '/pixel-deposit', 'App\Controllers\PixelController:deposit')->setName('pixel-deposit');

$app->get('/{slug}', 'App\Controllers\IncomingController');
