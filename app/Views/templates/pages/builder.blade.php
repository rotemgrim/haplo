@extends('layouts.html')
@section('head')
    <link rel="stylesheet" href="/css/builder.css">
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" id="bt-theme">
    <link rel="stylesheet" href="/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/bower_components/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css">
    <link rel="stylesheet" href="/bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css">
    <link rel="stylesheet" href="/bower_components/selectize/dist/css/selectize.bootstrap3.css">
    <link rel="stylesheet" href="/bower_components/jQuery-QueryBuilder/dist/css/query-builder.default.css" id="qb-theme">
@append
@section('body_scripts')
    <script src="/bower_components/jquery/dist/jquery.js"></script>
    <script src="/bower_components/jquery-ui/jquery-ui.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/bower_components/bootbox/bootbox.js"></script>
    <script src="/bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
    <script src="/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
    <script src="/bower_components/jquery-extendext/jQuery.extendext.min.js"></script>
    <script src="/bower_components/sql-parser/browser/sql-parser.js"></script>
    <script src="/bower_components/doT/doT.js"></script>
    <script src="/bower_components/jQuery-QueryBuilder/dist/js/query-builder.js"></script>
@append

@section('wrapper')
    <div class="container">
        <div class="col-md-12 col-lg-10 col-lg-offset-1">
            <div class="page-header">
                <h1>Mini API</h1>
                <div id="selector_server_div_id">
                    <label for="server_selector_id">Select Brand</label>
                    <select id="server_selector_id">
                        @foreach($servers_list as $server)
                            @if ($selected_server == $server['id'])
                                <option selected value="{{ $server['id'] }}">{{ $server['name'] }}</option>
                            @else
                                <option value="{{ $server['id'] }}">{{ $server['name'] }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div id="selector_process_div_id">
                    <label for="process_selector_id">Select Process</label>
                    <select id="process_selector_id">
                        @foreach($processes_list as $process)
                            @if ($selected_process == $process['id'])
                                {{--*/  $process_max_weight = $process['max_weight']; /*--}}
                                <option selected value="{{ $process['id'] }}">{{ $process['name'] }}</option>
                            @else
                                <option value="{{ $process['id'] }}">{{ $process['name'] }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div id="selector_btn_div_id">
                    <button id="selector_btn_id">Send</button>
                </div>
                <br><br><br>
                <button onclick="createBuilder();">Add builder</button>
            </div>

            Max weight: <div id="max_weight">  {{$process_max_weight}}</div>
            Total weight: <div id="total_weight"> </div>

            <div id="builders"></div>
        </div>
    </div>

    <button id="btn_calc_weights">Calc Weights</button>
    <button id="btn_save">SAVE ALL</button>
    <span id="btn_save_msg"></span>

    <textarea style="display: none;" id="builder_html">
    <div class="builder_outer" id="builder_outer_[id]">
        <div class="builder_unit" id="builder_[id]">
        </div>
        <div class="drag_label" id="drag_[id]">
        </div>
        <div class="total_weight" id="weight_[id]">
        </div>
        <div class="btn-group">
            <button class="btn btn-danger reset_[id]">Reset</button>
            <button class="btn btn-danger delete_[id]">Delete</button>
        </div>
        <div class="x">
            <label for="desc_[id]">Description</label>
            <tarea cols="50" rows="2" id="desc_[id]"></tarea>
            <label for="select_[id]">Action</label>
            <select id="select_[id]">
            @foreach($actions as $key=>$action)
               <option value="{{ str_replace("\\", "\\\\", $actions_classes_path).$action['value']}}">{{$action['title']}}</option>
            @endforeach
            </select>
        </div>
    </div>
</textarea>
    <script>
        var options = {
            allow_empty: true,
            //default_filter: 'state',
            optgroups: {
                core: {
                    en: 'Core'
                }
            },
            plugins: {
                'bt-tooltip-errors': { delay: 100},
                'sortable': null,
                'filter-description': { mode: 'bootbox' },
                'bt-selectpicker': null,
                'unique-filter': null,
                'bt-checkbox': { color: 'primary' },
                'invert': null
            },
            operators: [
                {type: 'equal',            optgroup: 'basic'},
                {type: 'not_equal',        optgroup: 'basic'},
                {type: 'in',               optgroup: 'basic'},
                {type: 'not_in',           optgroup: 'basic'},
                {type: 'less',             optgroup: 'numbers'},
                {type: 'less_or_equal',    optgroup: 'numbers'},
                {type: 'greater',          optgroup: 'numbers'},
                {type: 'greater_or_equal', optgroup: 'numbers'},
                {type: 'between',          optgroup: 'numbers'},
                {type: 'not_between',      optgroup: 'numbers'},
                {type: 'begins_with',      optgroup: 'strings'},
                {type: 'not_begins_with',  optgroup: 'strings'},
                {type: 'contains',         optgroup: 'strings'},
                {type: 'not_contains',     optgroup: 'strings'},
                {type: 'ends_with',        optgroup: 'strings'},
                {type: 'not_ends_with',    optgroup: 'strings'},
                {type: 'is_empty'     },
                {type: 'is_not_empty' },
                {type: 'is_null'      },
                {type: 'is_not_null'  }
            ],
            {!! $filters !!}
        };
        var init_jsons = [{!! $ccfile['json'] !!}];
        function saveCcfile(json_out, php_out) {
            var data = {
                ccfile_id: "{!! $ccfile['id'] !!}",
                ccfile_server_id: "{!! $ccfile['server_id'] !!}",
                ccfile_name: "{!! $ccfile['file'] !!}",
                ccfile_server_url: "{!! $ccfile['url'] !!}",
                ccfile_json: json_out,
                ccfile_content: php_out
            };
            var json = JSON.stringify(data);
            $.ajax({
                type: "POST",
                url: "/buildersave",
                data: json,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function(msg) {
                    $('#btn_save').html('Loading...');
                    $('#btn_save').attr('disabled','disabled');
                    $('#btn_save_msg').html('');
                },
                success: function(msg) {
                    $('#btn_save').html('SAVE ALL');
                    $('#btn_save').removeAttr('disabled');
                    $('#btn_save_msg').html('Saved!');
                },
                error: function(msg) {
                    alert(JSON.stringify(msg));
                }
            });
        }
    </script>
    <script src="/js/builder.js"></script>
@endsection


