@extends('layouts.html')

@section('head')
    <link rel="stylesheet" href="/css/admin.css">
    <link rel="stylesheet" href="/css/login.css">
@append

@section('bottom-scripts')
    <script>
        $("#login-button").click(function(event){
            event.preventDefault();
            var url = $('form').attr('action');
            $.ajax({
                method: "POST",
                url: url,
                dataType: "JSON",
                data: $('form').serialize(),
                beforeSend: function(){
                    $('form').fadeOut(500);
                },
                success: function(response){
                    console.log(response);
                    if(response.err === 0){
                        $('.wrapper').addClass('form-success');
                        setTimeout(function(){
                            window.location = response.result.destination;
                        },500);
                    } else {
                        $('.result').html(res.errors[0]);
                        setTimeout(function(){
                            $('form').fadeIn(500);
                        },500);
                    }

                }
            });

        });
    </script>
@append

@section('wrapper')
    <div class="wrapper">
        <div class="container">
            <h1>Welcome</h1>
            <form class="form" action="{{ App::router()->pathFor('login') }}" method="post">
                <input name="username" type="text" placeholder="Username">
                <input name="password" type="password" placeholder="Password">
                <button type="submit" id="login-button">Login</button>
                <div class="result"></div>
            </form>
        </div>

        <ul class="bg-bubbles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    </div>
@endsection

