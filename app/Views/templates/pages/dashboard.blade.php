@extends('layouts.adminPanel')

@section('content')
    <div id="content" class="col-sm-10" style="min-height: 955px;">

        <div>
            <hr>@include('partials._breadcrumbs')<hr>
        </div>

        <div class="row circleStats">
            @include('rubricks.circleStats')
        </div>

        <hr>

        <div class="row">

            <div class="col-md-4 col-sm-6">
                <div class="box">
                    <div class="box-header">
                        <h2><i class="fa fa-list"></i><span class="break"></span>Weekly Stat</h2>
                    </div>
                    <div class="box-content">
                        <ul class="dashboard-list">
                            <li>
                                <a href="index.html#">
                                    <i class="fa  fa-arrow-up green"></i>
                                    <span class="green">92</span>
                                    New Comments
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <i class="fa  fa-arrow-down red"></i>
                                    <span class="red">15</span>
                                    New Registrations
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <i class="fa  fa-minus blue"></i>
                                    <span class="blue">36</span>
                                    New Articles
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <i class="fa  fa-comment yellow"></i>
                                    <span class="yellow">45</span>
                                    User reviews
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <i class="fa  fa-arrow-up green"></i>
                                    <span class="green">112</span>
                                    New Comments
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <i class="fa  fa-arrow-down red"></i>
                                    <span class="red">31</span>
                                    New Registrations
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <i class="fa  fa-minus blue"></i>
                                    <span class="blue">93</span>
                                    New Articles
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <i class="fa  fa-comment yellow"></i>
                                    <span class="yellow">254</span>
                                    User reviews
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="box span4" ontablet="span6" ondesktop="span4">
                    <div class="box-header">
                        <h2><i class="fa fa-user"></i><span class="break"></span>Last Users</h2>
                        <div class="box-icon">
                            <a href="index.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        <ul class="dashboard-list">
                            <li>
                                <strong>Name:</strong> <a href="index.html#">Łukasz Holeczek</a><br>
                                <strong>Since:</strong> Jul 25, 2012 11:09<br>
                                <strong>Status:</strong> <span class="label label-success">Approved</span>
                            </li>
                            <li>
                                <strong>Name:</strong> <a href="index.html#">Bill Cole</a><br>
                                <strong>Since:</strong> Jul 25, 2012 11:09<br>
                                <strong>Status:</strong> <span class="label label-warning">Pending</span>
                            </li>
                            <li>
                                <strong>Name:</strong> <a href="index.html#">Jane Sanchez</a><br>
                                <strong>Since:</strong> Jul 25, 2012 11:09<br>
                                <strong>Status:</strong> <span class="label label-important">Banned</span>
                            </li>
                            <li>
                                <strong>Name:</strong> <a href="index.html#">Kate Presley</a><br>
                                <strong>Since:</strong> Jul 25, 2012 11:09<br>
                                <strong>Status:</strong> <span class="label label-info">Updates</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection

