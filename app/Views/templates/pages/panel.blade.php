@extends('layouts.adminPanel')

@section('content')

    <h1 class="page-header">Dashboard</h1>
    @include('partials._breadcrumbs', ['url'=>$breadcrumbs])

    <h2 class="sub-header"><i class="fa fa-list"></i> Section title</h2>
    <div class="row">
        <div class="col-lg-2">
            <input type="text" class="form-control" placeholder="Search for..." data-cip-id="cIPJQ342845648">
        </div>
        <div class="col-lg-10">
            <a href="#" class="btn btn-success pull-right">
                + Add {{ $addButton }}
            </a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    @foreach($headersArray as $header)
                        <th>{{$header}}</th>
                    @endforeach
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rowsArray as $row)
                    <tr>
                        @foreach($row as $cell)
                            <td>{{$cell}}</td>
                        @endforeach
                        <td>
                            {{--<span class="label label-default">Inactive</span>--}}
                            <a class="btn btn-info" href="{{ $edit.$row['id'] }}"><i class="fa fa-edit "></i></a>
                            <a class="btn btn-danger" href="{{ $del.$row['id'] }}"><i class="fa fa-trash-o "></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        @include('partials._pagination')

    </div>
@endsection

