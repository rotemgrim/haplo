<?php
    $temp = explode('/', $url);
    array_shift($temp);
?>
<ol class="breadcrumb">
@foreach($temp as $slug)
    <li><a href="{{ $slug }}">{{ ucfirst($slug) }}</a></li>
@endforeach
</ol>