<ul class="nav nav-sidebar">
    <li class="active"><a href="{{ App::router()->pathFor('dashboard') }}"><i class="fa fa-home icon"></i> Dashboard <span class="sr-only">(current)</span></a></li>
    <li><a href="{{ App::router()->pathFor('links') }}"><i class="fa fa-eye"></i> Links</a></li>
</ul>