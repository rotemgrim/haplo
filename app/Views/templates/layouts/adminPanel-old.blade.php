@extends('layouts.html')

@section('head')
    {{--<link rel="stylesheet" href="http://bootstrapmaster.com/live/perfectum/assets/css/bootstrap.min.css">--}}
    {{--<link rel="stylesheet" href="http://bootstrapmaster.com/live/perfectum/assets/css/style.min.css">--}}
    <link rel="stylesheet" href="/css/admin.css">
@append

@section('bottom-scripts')
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="http://bootstrapmaster.com/live/perfectum/assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="http://bootstrapmaster.com/live/perfectum/assets/js/bootstrap.min.js"></script>
    <script src="http://bootstrapmaster.com/live/perfectum/assets/js/jquery.knob.modified.min.js"></script>
    <script src="http://bootstrapmaster.com/live/perfectum/assets/js/pages/index.js"></script>
@append

@section('wrapper')


    {{--@include('partials._navbar')
    <div class="container-fluid">
        <div class="col-sm-2 main-menu-span">
            @include('partials._sidebar-nav')
        </div>
        <div class="col-sm-10">
            @yield('content')
        </div>
    </div>--}}

@endsection