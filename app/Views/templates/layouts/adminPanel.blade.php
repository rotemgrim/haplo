@extends('layouts.html')

@section('head')
    <link rel="stylesheet" href="/css/admin.css">
    <link rel="stylesheet" href="http://getbootstrap.com/examples/dashboard/dashboard.css">
@append

@section('bottom-scripts')
    {{--<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="http://bootstrapmaster.com/live/perfectum/assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="http://bootstrapmaster.com/live/perfectum/assets/js/bootstrap.min.js"></script>
    <script src="http://bootstrapmaster.com/live/perfectum/assets/js/jquery.knob.modified.min.js"></script>
    <script src="http://bootstrapmaster.com/live/perfectum/assets/js/pages/index.js"></script>--}}
@append

@section('wrapper')

    @include('partials._navbar')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2 col-md-1 sidebar">
                @include('partials._sidebar-nav')
            </div>
            <div class="col-sm-9 col-sm-offset-2 col-md-11 col-md-offset-1 main">
                @yield('content')
            </div>
        </div>
    </div>

@endsection