<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    @include('partials._head')

</head>
<body>


{{--@include('partials._bottom-scripts')--}}

    @yield('body_scripts')

    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    @yield('wrapper')

    @include('partials._foot')
</body>
</html>
