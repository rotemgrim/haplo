<?php
use Slim\Http\Request;
use Slim\Http\Response;

// permanently redirect paths with a trailing slash
// to their non-trailing counterpart
$app->add(function (Request $req, Response $res, callable $next) {
    $uri = $req->getUri();
    $path = $uri->getPath();
    if ($path != '/' && substr($path, -1) == '/') {
        $uri = $uri->withPath(substr($path, 0, -1));
        return $res->withRedirect((string)$uri, 301);
    }
    return $next($req, $res);
});


/**
 * This middleware checks the 'X-Forwarded-For', 'X-Forwarded', 'X-Cluster-Client-Ip', 'Client-Ip' headers for the first IP address it can
 * find. If none of the headers exist, or do not have a valid IP address, then the $_SERVER['REMOTE_ADDR'] is used.
 * Note that the proxy headers are only checked if the first parameter to the constructor is set to true. If set to false, then only
 * $_SERVER['REMOTE_ADDR'] is used
 */
$checkProxyHeaders = false;
$trustedProxies = ['10.0.0.1', '10.0.0.2'];
$app->add(new RKA\Middleware\IpAddress($checkProxyHeaders, $trustedProxies));




// App wide middleware that move to login.
/*$app->add(function (Request $req, Response $res, callable $next) {
    if (false) {
        return $res->withRedirect((string) App::router()->pathFor('login'), 302);
    }
    return $next($req, $res);
});*/

// Group size Middleware that move to login.
$adminAuthMiddleware = function (Request $req, Response $res, $next) {

    if(! \App\Models\DataMappers\User::isLogged())
        return $res->withRedirect((string) App::router()->pathFor('login'), 302);

    $response = $next($req, $res);
    //$response->getBody()->write('AFTER');
    return $response;
};

