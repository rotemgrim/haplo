<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production

        // DataBase settings
        'db' => [
            'dbname' => 'miniapi',
            'user' => 'root',
            'password' => '',
            'host' => 'localhost',
            'driver' => 'pdo_mysql',
        ],

        'engine' => [
            'actions_path' => '\\App\\Models\\Engine\\Actions\\',
            'ccfile_template' => '<?php namespace App\Models\Engine\CCFiles;
class [class_name]
{
    public static function run() {
        [conditions]
        [default_action]
    }
}'
        ],
        // Renderer settings
        'blade' => [
            'template_path' => __DIR__ . '/Views/templates',
            'cache_path' => __DIR__ . '/../cache/'
        ],

        // Monolog settings
        'logger' => [
            'name' => 'APP',
            'path' => __DIR__ . '/../logs/'.date('Y-m-d').'.log',
        ],

        // HashIds
        'hash' => [
            'salt' => 'someSalt',
            'min_size' => 8,
            'alphabet' => 'abcdefghijklmnopqrstuvwyz'
        ],
    ],


    //Override the default Not Found Handler
    'notFoundHandler' => function ($c) {
        return function ($request, $response) use ($c) {
            return $c['response']
                ->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write(View::make('layouts.404'));
        };
    }
];