<?php

$container = $app->getContainer();

// view renderer
$container['blade'] = function ($c) {
    $config = $c->get('settings')['blade'];
    $blade = new \Philo\Blade\Blade($config['template_path'], $config['cache_path']);
    return $blade;
};  View::init($container['blade']);

// monolog
$container['logger'] = function (\Slim\Container $c) {
    $config = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($config['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($config['path'], Monolog\Logger::DEBUG));
    return $logger;
};  Log::init($container['logger']);

// HashIds
$container['hash'] = function (\Slim\Container $c) {
    $conf= $c->get('settings')['hash'];
    $hashids = new Hashids\Hashids($conf['salt'], $conf['min_size'], $conf['alphabet']);
    return $hashids;
};

// DataBase
$container['DB'] = function (\Slim\Container $c) {
    $config = new \Doctrine\DBAL\Configuration();
    $conn = \Doctrine\DBAL\DriverManager::getConnection($c->get('settings')['db'], $config);
    return $conn;
};  DB::init($container['DB']);

//App::init($app);
App::preInit($container);