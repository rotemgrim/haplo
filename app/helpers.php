<?php

if (! function_exists('d')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function d()
    {
        array_map(function ($x) {
            (new \Illuminate\Support\Debug\Dumper())->dump($x);
        }, func_get_args());
    }
}

if (! function_exists('is_even')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  int
     * @return boolean
     */
    function is_even($int)
    {
        if($int % 2 == 0)
            return true;
        return false;
    }
}