<?php
require __DIR__ . '/../vendor/autoload.php';
//session_start();
\duncan3dc\Sessions\Session::name('App');


$app = new \Slim\App(require __DIR__ . '/../app/config.php');

// Set up Helpers
require __DIR__ . '/../app/helpers.php';

// Set up dependencies
require __DIR__ . '/../app/dependencies.php';

// Register middleware
require __DIR__ . '/../app/middleware.php';

// Register routes
require __DIR__ . '/../app/routes.php';

// Run app
$app->run();