var builders_counter = 0;
$('[data-toggle="tooltip"]').tooltip();
$('#btn_save').on('click', function() {
    var json_out = "";
    var php_out = "";
    var builders = $(".builder_unit").toArray();
    for(var i=0;i<builders.length; i++) {
        var id = builders[i].id.split("_");
        id = id[id.length-1];
        var as_json = $(builders[i]).queryBuilder('getRules', {get_flags: true});
        as_json.description = $('#desc_'+id).val();
        as_json.action = $('#select_'+id).val();
        json_out += JSON.stringify(as_json, undefined, 2) + ",";
        var as_php  = $(builders[i]).queryBuilder('getPHP', $(this).data('stmt'), false);
        php_out += "if (" + trimBoth(JSON.stringify(as_php.sql, undefined, 2)) + ") return " + as_json.action + ";\r\n\t\t";
    }
    json_out = json_out.substring(0, json_out.length - 1);
    saveCcfile(json_out, php_out);
});
function trimBoth(str) {return str.substring(1, str.length - 1);}

function createBuilder() {
    var id = builders_counter;
    var builder_html = $('#builder_html').val().split('[id]').join(id).split('tarea').join('textarea');
    $('#builders').append(builder_html);
    var $b = $('#builder_'+id);
    $b.queryBuilder(options);
    $b.on('afterCreateRuleInput.queryBuilder', function(e, rule) {
        if (rule.filter.plugin == 'selectize') {
            rule.$el.find('.rule-value-container').css('min-width', '200px')
                .find('.selectize-control').removeClass('form-control');
        }
    });

    $b.on('afterUpdateRuleFilter.queryBuilder', calc_weights);
    $b.on('afterDeleteRule.queryBuilder', calc_weights);

    $('.reset_'+id).on('click', function() {
        $('#builder_'+id).queryBuilder('reset');
        $('#result_'+id).addClass('hide').find('pre').empty();
    });
    $('.delete_'+id).on('click', function() {
        $('#builder_outer_'+id).remove();
        calc_weights();
    });
    $('.parse-json_'+id).on('click', function() {
        $('#result_'+id).removeClass('hide')
            .find('pre').html(JSON.stringify(
            $('#builder_'+id).queryBuilder('getRules', {get_flags: true}),
            undefined, 2
        ));
    });
    $('.parse-php_'+id).on('click', function() {
        var res = $('#builder_'+id).queryBuilder('getPHP', $(this).data('stmt'), false);
        $('#result_'+id).removeClass('hide')
            .find('pre').html(
            res.sql + (res.params ? '\n\n' + JSON.stringify(res.params, undefined, 2) : '')
        );
    });
    builders_counter++;
}
function calc_weights() {
    var builders = $(".builder_unit").toArray();
    var total_weight = 0;
    for(var i=0;i<builders.length; i++) {
        var id = builders[i].id.split("_");
        id = id[id.length-1];
        var filters = $(builders[i]).find('.rule-filter-container .filter-option');

        var unit_weight = 0;
        for(var j=0;j<filters.length; j++) {
            //alert(id + " " + $(filters[j]).html());
            for(var k=0; k < options.filters.length; k++) {
                if (options.filters[k].label.en == $(filters[j]).html()) {
                    unit_weight += 1*options.filters[k].weight;
                    break;
                }
            }
        }
        total_weight+=unit_weight;
        $("#weight_" + id).html("<br> Weight: " + unit_weight);
    }
    $('#total_weight').html(total_weight);
}

function initBuilders() {
    for(var i=0; i<init_jsons.length; i++) {
        createBuilder();
        var b = $('#builder_' + i);
        $(b).queryBuilder('setRules', init_jsons[i]);
        //$(b).on('afterUpdateRuleFilter.queryBuilder', calc_weights);
        //$(b).on('afterDeleteRule.queryBuilder', calc_weights);
        $('#desc_' + i).val(init_jsons[i].description);
        $('#select_' + i).val(init_jsons[i].action.split("\\").join("\\\\"));
    }
    calc_weights();
}

$( document ).ready(function() {
    initBuilders();
    $('#selector_btn_id').click(function () {window.location.href = "/builder/" + $('#server_selector_id').val() + "/" + $('#process_selector_id').val();});
    $('#builders').sortable({ handle: '.drag_label' });
});
    